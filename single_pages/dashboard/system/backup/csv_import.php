<?php  defined('C5_EXECUTE') or die("Access Denied.");?>

<form action="<?php  echo $this->action('process')?>" method="post" class="form-horizontal">
  <?php echo $this->controller->token->output('import')?>
  <fieldset>
    <legend><?php  echo t('CSV Import'); ?></legend>
    <div class="control-group">
      <?php  echo $form->label('fID', t('Select CSV File')); ?>
      <div class="controls">
        <?php  $al = Core::make('helper/concrete/asset_library'); ?>
        <?php  echo $al->text('fID', 'fID', 'Select File', $f); ?>
      </div>
    </div>
  </fieldset>
  <?php if ($results) { ?>
  <fieldset>
    <div class="alert">
      <?php echo implode("<br />", $results) ?>
    </div>
  </fieldset>
  <?php } ?>
  <div class="ccm-dashboard-form-actions-wrapper">
    <div class="ccm-dashboard-form-actions">
      <button class="pull-right btn btn-success" type="submit" ><?php  echo t('Import')?></button>
    </div>
  </div>
</form>