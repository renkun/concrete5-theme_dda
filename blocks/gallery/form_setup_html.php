<?php  defined('C5_EXECUTE') or die("Access Denied.");

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();
$pageSelector = Loader::helper('form/page_selector');
?>
<script>
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Loader::helper('validation/token')->generate('editor')?>";
    $(document).ready(function(){
        var ccmReceivingEntry = '';
        var sliderEntriesContainer = $('.ccm-gallery-entries');
        var _templateSlide = _.template($('#imageTemplate').html());
        var attachDelete = function($obj) {
            $obj.click(function(){
                var deleteIt = confirm('<?php echo t('Are you sure?') ?>');
                if(deleteIt == true) {
                    $(this).closest('.ccm-gallery-entry').remove();
                    doSortCount();
                }
            });
        }

        var attachSortDesc = function($obj) {
            $obj.click(function(){
               var myContainer = $(this).closest($('.ccm-gallery-entry'));
               myContainer.insertAfter(myContainer.next('.ccm-gallery-entry'));
               doSortCount();
            });
        }

        var attachSortAsc = function($obj) {
            $obj.click(function(){
                var myContainer = $(this).closest($('.ccm-gallery-entry'));
                myContainer.insertBefore(myContainer.prev('.ccm-gallery-entry'));
                doSortCount();
            });
        }

        var attachFileManagerLaunch = function($obj) {
            $obj.click(function(){
                var oldLauncher = $(this);
                ConcreteFileManager.launchDialog(function (data) {
                    ConcreteFileManager.getFileDetails(data.fID, function(r) {
                        jQuery.fn.dialog.hideLoader();
                        var file = r.files[0];
                        oldLauncher.html(file.resultsThumbnailImg);
                        oldLauncher.next('.image-fID').val(file.fID)
                    });
                });
            });
        }

        var doSortCount = function(){
            $('.ccm-gallery-entry').each(function(index) {
                $(this).find('.ccm-gallery-entry-sort').val(index);
            });
        };

        var makeID = function() {
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
          for( var i=0; i < 10; i++ )
              text += possible.charAt(Math.floor(Math.random() * possible.length));
          return text;
        }



       <?php if($rows) {
           foreach ($rows as $row) { ?>
           sliderEntriesContainer.append(_templateSlide({
                image: '<?php echo $row['image'] ?>',
                <?php if(File::getByID($row['image'])) { ?>
                image_url: '<?php echo File::getByID($row['image'])->getThumbnailURL('file_manager_listing');?>',
                <?php } else { ?>
                image_url: '',
               <?php } ?>
               hoverImage: '<?php echo $row['hoverImage'] ?>',
                <?php if(File::getByID($row['hoverImage'])) { ?>
                hover_image_url: '<?php echo File::getByID($row['hoverImage'])->getThumbnailURL('file_manager_listing');?>',
                <?php } else { ?>
                hover_image_url: '',
               <?php } ?>
                height: '<?php echo $row['height'] ?>',
                <?php if($row['linkedPageCID']){
                  $page = Page::getByID($row['linkedPageCID']);
                  $pageName = $page->getCollectionName();
                }
                ?>
                linkedPageCID: '<?php echo $row['linkedPageCID'] ?>',
                pageName: '<?=$pageName?>',
                sort_order: '<?php echo $row['sortOrder'] ?>',
            }));

        <?php }
        }?>

        doSortCount();

        $('.ccm-add-gallery-entry').click(function(){
           var thisModal = $(this).closest('.ui-dialog-content');
            sliderEntriesContainer.append(_templateSlide({
                image: '',
                hoverImage: '',
                height: '',
                linkedPageCID: '',
                pageName: '',
                sort_order: '',
                image_url: '',
                hover_image_url: ''
            }));

            var newSlide = $('.ccm-gallery-entry').last();
            thisModal.scrollTop(newSlide.offset().top);
            attachDelete(newSlide.find('.ccm-delete-gallery-entry'));
            attachFileManagerLaunch(newSlide.find('.ccm-pick-slide-image'));
            newSlide.find('div[data-field=entry-link-page-selector-select]').concretePageSelector({
                'inputName': 'internalLinkCID[]'
            });
            attachSortDesc(newSlide.find('i.fa-sort-desc'));
            attachSortAsc(newSlide.find('i.fa-sort-asc'));
            doSortCount();
        });
        attachDelete($('.ccm-delete-gallery-entry'));
        attachSortAsc($('i.fa-sort-asc'));
        attachSortDesc($('i.fa-sort-desc'));
        attachFileManagerLaunch($('.ccm-pick-slide-image'));
    });
</script>
<style>

    .ccm-gallery-block-container .redactor_editor {
        padding: 20px;
    }
    .ccm-gallery-block-container input[type="text"],
    .ccm-gallery-block-container textarea {
        display: block;
        width: 100%;
    }
    .ccm-gallery-block-container .btn-success {
        margin-bottom: 20px;
    }

    .ccm-gallery-entries {
        padding-bottom: 30px;
    }

    .ccm-pick-slide-image {
        padding: 15px;
        cursor: pointer;
        background: #dedede;
        border: 1px solid #cdcdcd;
        text-align: center;
        vertical-align: center;
    }

    .ccm-pick-slide-image img {
        max-width: 100%;
    }

    .ccm-gallery-entry {
        position: relative;
    }

    .ccm-gallery-block-container i.fa-sort-asc {
        position: absolute;
        top: 10px;
        right: 10px;
        cursor: pointer;
    }

    .ccm-gallery-block-container i:hover {
        color: #5cb85c;
    }

    .ccm-gallery-block-container i.fa-sort-desc {
        position: absolute;
        top: 15px;
        cursor: pointer;
        right: 10px;
    }
    .ccm-gallery-block-container .help {
        font-size: 80%;
        font-style: italic;
    }
    .ccm-gallery-block-container table {
      width: 100%;
    }
    .ccm-gallery-block-container .help {
        font-size: 80%;
        font-style: italic;
    }
</style>
<div class="ccm-gallery-block-container">
  <fieldset>
    <legend>Settings</legend>
    <div class="form-group">
      <table>
        <tr>
          <td>
            <label><?php echo t('Columns LG') ?></label>
            <select name="columnsLg">
              <option value="1"<?php if ($columnsLg == 1) { ?> selected<?php } ?>>1</option>
              <option value="2"<?php if ($columnsLg == 2) { ?> selected<?php } ?>>2</option>
              <option value="3"<?php if ($columnsLg == 3) { ?> selected<?php } ?>>3</option>
              <option value="4"<?php if ($columnsLg == 4) { ?> selected<?php } ?>>4</option>
              <option value="5"<?php if ($columnsLg == 5) { ?> selected<?php } ?>>5</option>
              <option value="6"<?php if ($columnsLg == 6) { ?> selected<?php } ?>>6</option>
              <option value="7"<?php if ($columnsLg == 7) { ?> selected<?php } ?>>7</option>
              <option value="8"<?php if ($columnsLg == 8) { ?> selected<?php } ?>>8</option>
              <option value="9"<?php if ($columnsLg == 9) { ?> selected<?php } ?>>9</option>
              <option value="10"<?php if ($columnsLg == 10) { ?> selected<?php } ?>>10</option>
              <option value="11"<?php if ($columnsLg == 11) { ?> selected<?php } ?>>11</option>
              <option value="12"<?php if ($columnsLg == 12) { ?> selected<?php } ?>>12</option>
            </select>
          </td>
          <td>
            <label><?php echo t('Columns MD') ?></label>
            <select name="columnsMd">
              <option value="1"<?php if ($columnsMd == 1) { ?> selected<?php } ?>>1</option>
              <option value="2"<?php if ($columnsMd == 2) { ?> selected<?php } ?>>2</option>
              <option value="3"<?php if ($columnsMd == 3) { ?> selected<?php } ?>>3</option>
              <option value="4"<?php if ($columnsMd == 4) { ?> selected<?php } ?>>4</option>
              <option value="5"<?php if ($columnsMd == 5) { ?> selected<?php } ?>>5</option>
              <option value="6"<?php if ($columnsMd == 6) { ?> selected<?php } ?>>6</option>
              <option value="7"<?php if ($columnsMd == 7) { ?> selected<?php } ?>>7</option>
              <option value="8"<?php if ($columnsMd == 8) { ?> selected<?php } ?>>8</option>
              <option value="9"<?php if ($columnsMd == 9) { ?> selected<?php } ?>>9</option>
              <option value="10"<?php if ($columnsMd == 10) { ?> selected<?php } ?>>10</option>
              <option value="11"<?php if ($columnsMd == 11) { ?> selected<?php } ?>>11</option>
              <option value="12"<?php if ($columnsMd == 12) { ?> selected<?php } ?>>12</option>
            </select>
          </td>
          <td>
            <label><?php echo t('Columns SM') ?></label>
            <select name="columnsSm">
              <option value="1"<?php if ($columnsSm == 1) { ?> selected<?php } ?>>1</option>
              <option value="2"<?php if ($columnsSm == 2) { ?> selected<?php } ?>>2</option>
              <option value="3"<?php if ($columnsSm == 3) { ?> selected<?php } ?>>3</option>
              <option value="4"<?php if ($columnsSm == 4) { ?> selected<?php } ?>>4</option>
              <option value="5"<?php if ($columnsSm == 5) { ?> selected<?php } ?>>5</option>
              <option value="6"<?php if ($columnsSm == 6) { ?> selected<?php } ?>>6</option>
              <option value="7"<?php if ($columnsSm == 7) { ?> selected<?php } ?>>7</option>
              <option value="8"<?php if ($columnsSm == 8) { ?> selected<?php } ?>>8</option>
              <option value="9"<?php if ($columnsSm == 9) { ?> selected<?php } ?>>9</option>
              <option value="10"<?php if ($columnsSm == 10) { ?> selected<?php } ?>>10</option>
              <option value="11"<?php if ($columnsSm == 11) { ?> selected<?php } ?>>11</option>
              <option value="12"<?php if ($columnsSm == 12) { ?> selected<?php } ?>>12</option>
            </select>
          </td>
        </tr>
      </table>
      <div class="help">Note: XS size will always be one column.</div>
    </div>
    <div class="form-group">
      <label><?php echo t('Enable Lightbox')?></label>
      <select name="lightbox" style="vertical-align: middle">
        <option value="0"<?php if ($lightbox == 0) { ?> selected<?php } ?>>No</option>
        <option value="1"<?php if ($lightbox == 1) { ?> selected<?php } ?>>Yes</option>
      </select>
    </div>
  </fieldset>
  <fieldset>
    <legend>Images</legend>
    <span class="btn btn-success ccm-add-gallery-entry"><?php echo t('Add Image') ?></span>
    <div class="ccm-gallery-entries">

    </div>
  </fieldset>
</div>
<script type="text/template" id="imageTemplate">
    <div class="ccm-gallery-entry well">
        <i class="fa fa-sort-desc"></i>
        <i class="fa fa-sort-asc"></i>
        <div class="form-group">
            <label><?php echo t('Image') ?></label>
            <div class="ccm-pick-slide-image">
                <% if (image_url.length > 0) { %>
                    <img src="<%= image_url %>" />
                <% } else { %>
                    <i class="fa fa-picture-o"></i>
                <% } %>
            </div>
            <input type="hidden" name="<?php echo $view->field('image')?>[]" class="image-fID" value="<%=image%>" />
        </div>
        <div class="form-group">
            <label><?php echo t('Hover Image') ?></label>
            <div class="ccm-pick-slide-image">
                <% if (hover_image_url.length > 0) { %>
                    <img src="<%= hover_image_url %>" />
                <% } else { %>
                    <i class="fa fa-picture-o"></i>
                <% } %>
            </div>
            <input type="hidden" name="<?php echo $view->field('hoverImage')?>[]" class="image-fID" value="<%=hoverImage%>" />
        </div>
        <!-- PAGE SELECTOR --->
        <div class="form-group">
          <label><?=t('Select a Page')?></label>
          <div id="select-page-<%=sort_order%>">
            <?php $this->inc('elements/page_selector.php');?>
          </div>
        </div>
        <input class="ccm-gallery-entry-sort" type="hidden" name="<?php echo $view->field('sortOrder')?>[]" value="<%=sort_order%>"/>
        <div class="form-group">
            <span class="btn btn-danger ccm-delete-gallery-entry"><?php echo t('Delete Entry'); ?></span>
        </div>
    </div>
</script>


