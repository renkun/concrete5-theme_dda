<?php
  defined('C5_EXECUTE') or die("Access Denied."); 
  $ih = Core::make('helper/image');
  $maxWidth = 400;
  $maxHeight = 400;
?>

<div class="row img-gallery">
  <?php 
  $count = 0;
  foreach($images as $imgInfo) {
    $f = File::getByID($imgInfo['image']);
    $thumb = $ih->getThumbnail($f, $maxWidth, $maxHeight);
    if ($imgInfo['hoverImage']) {
      $h = File::getByID($imgInfo['hoverImage']);
      $hoverThumb = $ih->getThumbnail($h, $maxWidth, $maxHeight);
    }
  ?>
  <div class="col-sm-<?php echo intval(12/$columnsSm); ?> col-md-<?php echo intval(12/$columnsMd); ?> col-lg-<?php echo intval(12/$columnsLg); ?> gallery-image-<?php echo $bID ?>">
  <?php
    $img = '<img src="' . $thumb->src . '" class="img-responsive gallery-item" data-default-url="'. $thumb->src . '" ';
    if ($imgInfo['hoverImage'])
      $img .= 'data-hover-url="'.$hoverThumb->src.'" ';
    $img .= '/>';
    
    if ($lightbox) {
      echo '<a class="gallery-lb-'.$bID.'" href="'.($imgInfo['hoverImage'] ? $h->getRelativePath() : $f->getRelativePath()).'">'.$img.'</a>';
    }
    else if ($imgInfo['linkedPageCID']) {
      $page = Page::getByID($imgInfo['linkedPageCID']);
      echo '<a href="'.$page->getCollectionPath().'">' . $img . '</a>';
    }
    else
      echo $img;
  ?>
    </div>
<?php 
    $count++;
  } ?>
</div>

<script type="text/javascript">
$(document).ready(function() {
  $('.gallery-item').each(function(i, obj) {
    if ($(obj).data('hover-url')) {
      $(obj).mouseover(function(e) {$(this).attr("src", $(this).data('hover-url'))})
            .mouseout(function(e) {$(this).attr("src", $(this).data('default-url'))});
    }
  });
  $('a.gallery-lb-<?php echo $bID ?>').magnificPopup({
    type:'image',
    gallery:{enabled:true}
  });
  $('.gallery-image-<?php echo $bID ?>').matchHeight();
});
</script>