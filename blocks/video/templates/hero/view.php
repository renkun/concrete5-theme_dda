<?php
defined('C5_EXECUTE') or die("Access Denied.");

// now that we're in the specialized content file for this block type, 
// we'll include this block type's class, and pass the block to it, and get
// the content

?>
<div id="hero-video">
  <div class="embed-responsive embed-responsive-16by9">
  <?php
  $c = Page::getCurrentPage();
  //$vWidth=intval($controller->width);
  //$vHeight=intval($controller->height);
  if ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 300px;">
      <div style="padding:8px 0px; padding-top: 140px;"><?php echo t('Content disabled in edit mode.')?></div>
    </div>
  <?php }else if (!$webmURL && !$oggURL && !$mp4URL) { ?>
      <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 300px;">
      <div style="padding:8px 0px; padding-top: 140px;"><?php echo t('No Video Files Selected.')?></div>
      </div>
  <?php } else if ($webmURL || $oggURL || $mp4URL){ ?>
      <video autoplay preload="auto" loop="loop" muted="muted" volume="0" <?php echo $posterURL ? 'poster="'.$posterURL.'"' : '' ?> class="embed-responsive-item">
          <?php if($webmURL) { ?>
          <source src="<?php echo $webmURL ?>" type="video/webm" />
          <?php }
          if ($oggURL) { ?>
          <source src="<?php echo $oggURL ?>" type="video/ogg" />
          <?php }
          if ($mp4URL) { ?>
              <source src="<?php echo $mp4URL ?>" type="video/mp4" />
        <?php // quicktime player for older IE ?>
      <object CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" width="100%" height="auto" CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">
        <param name="src" value="<?php echo $mp4URL?>">
        <param name="autoplay" value="true">
        <param name="loop" value="false">
        <param name="controller" value="true">
        <embed src="<?php echo $mp4URL?>" width="100%" height="auto" autoplay="true" loop="false" controller="true" pluginspage="http://www.apple.com/quicktime/"></embed>
      </object>
          <?php } ?>
  <?php } ?>
     </video>
   </div>
</div>