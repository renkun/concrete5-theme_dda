<?php   defined('C5_EXECUTE') or die(_("Access Denied."));?>
<div id="home-featured-events">
<?php  
$c = Page::getCurrentPage();
$imgHelper = Loader::helper('image');
if (count($eArray) > 0) {
  foreach ($eArray as $date_string => $event) {
    extract($eventify->getEventListVars($event)); ?>
    <div class="event">
    <?php
      $imageF = $event->getAttribute('thumbnail');
      if (isset($imageF)) { 
        $image = $imgHelper->getThumbnail($imageF, 500, 150)->src; 
        echo '<div class="event-image" style="background-image: url(\''.$image.'\')"></div>';
      }
    ?>
      <h5>Featured Event</h5>
      <h3><?php echo $title ?></h3>
      <h5><?php 
        echo date(t('m.d'), strtotime($date)); 
        if (is_array($next_dates_array)) {
          foreach ($next_dates_array as $next_date) {
            if ($allday != 1) {
              echo "&nbsp;-&nbsp;";
              echo date(t('g:i a'), strtotime($next_date->start)) . ' - ' . date(t('g:i a'), strtotime($next_date->end)) . '<br/>';
            }
          }
        }
      ?></h5>
      <p>
      <?php  
        if ($truncateChars) {
          print  substr($content, 0, $truncateChars) . '.....';
        } else {
          print  $content;
        }
      ?>
      </p>
      <?php
        echo '<a href="' . $url;
        if (!$grouped) {
          echo '?eID=' . $eID;
        }
        echo '" class="btn btn-primary">Learn More</a>';
      ?>

    </div>
<?php  }
} else {
    echo '<p>' . $nonelistmsg . '</p>';
}
?>
</div>