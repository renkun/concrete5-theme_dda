<?php defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$kml_file = $controller->getFileObject();
?>
<div class="kml-map">
<?php if ($c->isEditMode()) { ?>
  <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
    <div style="padding: 80px 0px 0px 0px"><?php echo t('Google Map disabled in edit mode.')?></div>
  </div>
<?php  } else if (!is_object($kml_file)){ ?>  
  <div class="ccm-edit-mode-disabled-item" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>">
    <div style="padding: 80px 0px 0px 0px"><?php echo t('KML Map disabled - File not found.')?></div>
  </div>
<?php  } else { ?>
  <?php  if( strlen($title)>0){ ?><h4><?php echo $title?></h4><?php  } ?>
  <a id="geoxml<?php echo $ident?>"/>
  <div id="googleMapCanvas<?php echo $ident?>" class="googleMapCanvas" style="height: <?php echo $height; ?>"></div>
  <div id="sidebarCanvas<?php echo $ident?>" class="map-sidebar"></div>
<?php  } ?>
</div>


<?php
/*
    Note - this goes in here because it's the only way to preserve block caching for this block. We can't
    set these values through the controller
*/
?>

<?php if (!$c->isEditMode() && is_object($kml_file)): ?>
<script type="text/javascript">
  var stylesArray = [
    {
      "featureType": "road.local",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "featureType": "road.arterial",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "featureType": "road.highway",
      "stylers": [
        { "color": "#ffffff" }
      ]
    },{
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#005D73" }
      ]
    },{
      "featureType": "water",
      "stylers": [
        { "color": "#37a7cc" }
      ]
    },{
      "featureType": "poi.park",
      "stylers": [
        { "color": "#aad383" }
      ]
    },{
      "featureType": "landscape",
      "stylers": [
        { "color": "#d8d2c1" }
      ]
    },{
      "featureType": "poi.business",
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#00766C" }
      ]
    },{
      "featureType": "poi.park",
      "elementType": "labels.text.fill",
      "stylers": [
        { "color": "#00766C" }
      ]
    },{
      "featureType": "transit.line",
      "stylers": [
        { "color": "#ffffff" }
      ]
    }
  ]
  var map_<?php echo $ident?>;
  var layers_<?php echo $ident?> = [];
  function googleMapInit<?php echo $ident?>() {
    try{
      var mapOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: false,
        scrollwheel: <?php echo !!$scrollwheel ? "true" : "false"?>,
        mapTypeControl: false,
        styles: stylesArray
      };
      map_<?php echo $ident?> = new google.maps.Map(document.getElementById('googleMapCanvas<?php echo $ident?>'), mapOptions);
    }catch(e){
      $("#googleMapCanvas<?php echo $ident?>").replaceWith("<p>Unable to display map: "+e.message+"</p>")
    }
  }
  function initGeoXML<?php echo $ident?>() {
    geoxml<?php echo $ident?> = new GeoXml(
      "geoxml<?php echo $ident?>",
      map_<?php echo $ident?>,
      "<?php echo $kml_file->getRelativePath() ?>",
      {
        dohilite: false,
        sidebarid: "sidebarCanvas<?php echo $ident?>",
        sidebariconheight: 20,
        quiet: true,
        publishdirectory: "<?php echo $block_path ?>",
        simplelegend: true,
        maxiwwidth: 400,
        titlestyle: 'style = "font-family: alternate-gothic-no-1-d, Helvetica, Arial, sans-serif; font-size: 25px; text-transform: uppercase; font-weight: bold;"',
        descstyle: 'style = "font-family: proxima-nova, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal;"'
      }
    );
    geoxml<?php echo $ident?>.parse();
  }
  $(function() {
    var t;
    var startWhenVisible = function (){
      if ($("#googleMapCanvas<?php echo $ident?>").is(":visible")){
        window.clearInterval(t);
        googleMapInit<?php echo $ident?>();
        initGeoXML<?php echo $ident?>();
        return true;
      }
      return false;
    };
    if (!startWhenVisible()) {
      t = window.setInterval(function(){startWhenVisible();},100);
    }
  });
</script>
<?php endif; ?>
