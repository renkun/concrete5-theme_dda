<?php defined('C5_EXECUTE') or die("Access Denied."); 
    $al = Loader::helper('concrete/asset_library');
    $bf = null;
    if ($controller->getFileID() > 0) {
        $bf = $controller->getFileObject();
    }
?>

<style>
    .ccm-kml-map-block-container .redactor_editor {
        padding: 20px;
    }
    .ccm-kml-map-block-container input[type="text"],
    .ccm-kml-map-block-container textarea {
        display: block;
        width: 100%;
    }
    .ccm-kml-map-block-container .btn-success {
        margin-bottom: 20px;
    }

    .ccm-kml-map-entries {
        padding-bottom: 30px;
    }

    .ccm-kml-map-entry {
        position: relative;
    }

    .ccm-kml-map-block-container i.fa-sort-asc {
        position: absolute;
        top: 10px;
        right: 10px;
        cursor: pointer;
    }

    .ccm-kml-map-block-container i:hover {
        color: #5cb85c;
    }

    .ccm-kml-map-block-container i.fa-sort-desc {
        position: absolute;
        top: 15px;
        cursor: pointer;
        right: 10px;
    }
    .ccm-kml-map-block-container .help {
        font-size: 80%;
        font-style: italic;
    }
    .ccm-kml-map-block-container table {
      width: 100%;
    }
    .ccm-kml-map-block-container .help {
        font-size: 80%;
        font-style: italic;
    }
</style>


<div class="ccm-kml-map-block-container row">
    <div class="col-xs-12">
        <div class="form-group">
            <?php echo $form->label('title', t('Map Title (optional)'));?>
            <?php echo $form->text('title', $mapObj->title);?>
        </div>
    </div>

    <div class="col-xs-4">  
        <div class="form-group"> 
            <?php echo $form->label('width', t('Map Width'));?>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
                <?php if(is_null($width) || $width == 0) {$width = '100%';};?>
                <?php echo $form->text('width', $width);?>
            </div>
        </div>
    </div>

    <div class="col-xs-4">
        <div class="form-group">
            <?php echo $form->label('height', t('Map Height'));?>
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-arrows-v"></i></span>
                <?php if(is_null($height) || $height == 0) {$height = '400px';};?>
                <?php echo $form->text('height', $height); ?>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
          <label>
            <?php echo $form->checkbox('scrollwheel', 1, (is_null($scrollwheel) || $scrollwheel)); ?>
            <?php echo t("Enable Scroll Wheel")?>
          </label>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
          <label>
            <?php echo $form->label('kmlFileID', t('KML File'))?>
            <?php echo $al->file('ccm-b-file', 'kmlFileID', t('Choose File'), $bf);?>
          </label>
        </div>
    </div>
    
</div>


<script type="text/javascript">
$(function() {
    window.C5GMaps.init();
});
</script>