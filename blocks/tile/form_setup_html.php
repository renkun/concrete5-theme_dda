<?php  
defined('C5_EXECUTE') or die("Access Denied.");
$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();
$al = Loader::helper('concrete/asset_library');
$bf = null;
$pageSelector = Loader::helper('form/page_selector');

if ($controller->getFileID() > 0) { 
  $bf = $controller->getFileObject();
}
$args = array();
$constrain = $maxWidth > 0 || $maxHeight > 0;
if ($maxWidth == 0) {
    $maxWidth = '';
}
if ($maxHeight == 0) {
    $maxHeight = '';
}
?>

<fieldset>
  <div class="form-group">
    <label class="control-label"><?php echo t('Image')?></label>
    <?php echo $al->image('ccm-b-image', 'fID', t('Choose Image'), $bf, $args);?>
  </div>
  <div class="form-group">
    <label><?php echo t('Content') ?></label>
    <div class="redactor-edit-content"></div>
    <textarea style="display: none" class="redactor-content" name="content"><?php echo $content ?></textarea>
  </div>
</fieldset>
<fieldset>
  <legend>Call To Action</legend>
    <div class="form-group">
      <?php echo $form->label('ctaText', t('Button Label')) ?>
      <?php echo $form->text('ctaText', $ctaText); ?>
    </div>
    <div class="form-group">
      <?php echo $form->label('internalLinkCID', t('Choose Page:')) ?>
      <?php echo $pageSelector->selectPage('internalLinkCID', $internalLinkCID); ?>
    </div>
    <div class="form-group">
      <?php echo $form->label('color', t('Color:')) ?>
      <select name="background">
        <option value="white" <?php echo (empty($background) || $background == 'white') ? 'selected="selected"' : ''; ?>>White</option>
        <option value="dark" <?php echo ($background == 'dark') ? 'selected="selected"' : ''; ?>>Dark</option>
      </select>
    </div>
</fieldset>

<script>
  $(function() {  // activate redactors
      $('.redactor-content').redactor({
          height: '200',
          'concrete5': {
              filemanager: <?php echo $fp->canAccessFileManager()?>,
              sitemap: <?php echo $tp->canAccessSitemap()?>,
              lightbox: true
          },
          'plugins': ['concrete5', 'underline']
      });
  });
</script>