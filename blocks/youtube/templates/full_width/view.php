<?php
defined('C5_EXECUTE') or die("Access Denied.");

$url       = parse_url($videoURL);
$pathParts = explode('/', rtrim($url['path'], '/'));
$videoID   = end($pathParts);

if (isset($url['query'])) {
  parse_str($url['query'], $query);
  $videoID = (isset($query['v'])) ? $query['v'] : $videoID;
}

$vWidth  = ($vWidth)  ? $vWidth  : 425;
$vHeight = ($vHeight) ? $vHeight : 344;

if (Page::getCurrentPage()->isEditMode()) { ?>

  <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: <?php echo $vHeight; ?>px; margin: 0 auto;">
    <div style="padding:8px 0px; padding-top: <?php echo round($vHeight/2)-10; ?>px;"><?php echo t('YouTube Video disabled in edit mode.'); ?></div>
  </div>
  
<?php } elseif ($vPlayer == 1) { ?>

  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/<?php echo $videoID; ?>?wmode=transparent" frameborder="0" allowfullscreen></iframe>
  </div>
  
<?php } else { ?>

  <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: <?php echo $vHeight; ?>px; margin: 0 auto;">
    <div style="padding:8px 0px; padding-top: <?php echo round($vHeight/2)-10; ?>px;"><?php echo t('This template is not compatible with Flash mode. Please use iframe or another view.'); ?></div>
  </div>
  
<?php } ?>
