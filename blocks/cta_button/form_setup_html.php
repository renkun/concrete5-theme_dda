<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<fieldset>
  <legend><?php echo t('Link')?></legend>
  <div class="form-group">
    <select name="linkType" data-select="feature-link-type" class="form-control">
      <option value="1" <?php echo (empty($externalLink) && !empty($internalLinkCID) ? 'selected="selected"' : '')?>><?php echo t('Another Page')?></option>
      <option value="2" <?php echo (!empty($externalLink) ? 'selected="selected"' : '')?>><?php echo t('External URL')?></option>
    </select>
  </div>
  <div data-select-contents="feature-link-type-internal" style="display: none;" class="form-group">
    <?php echo $form->label('internalLinkCID', t('Choose Page:'))?>
    <?php echo Loader::helper('form/page_selector')->selectPage('internalLinkCID', $internalLinkCID); ?>
  </div>
  <div data-select-contents="feature-link-type-external" style="display: none;" class="form-group">
    <?php echo $form->label('externalLink', t('URL'))?>
    <?php echo $form->text('externalLink', $externalLink); ?>
  </div>
</fieldset>

<fieldset>
  <legend><?php echo t('Appearance')?></legend>
  <div class="form-group">
    <?php echo $form->label('text', t('Text')); ?>
    <?php echo $form->text('text', $text); ?>
  </div>
  <div class="form-group">
    <?php echo $form->label('text', t('Text Line 2')); ?>
    <div class="help">(Used in sidebar context only)</div>
    <?php echo $form->text('textLine2', $textLine2); ?>
  </div>
  <div class="form-group">
    <?php echo $form->label('position', t('Position')); ?>
    <select name="alignment">
      <option value="left" <?php echo ($alignment == 'left') ? 'selected="selected"' : ''; ?>>Left</option>
      <option value="center" <?php echo ($alignment == 'center') ? 'selected="selected"' : ''; ?>>Center</option>
      <option value="right" <?php echo ($alignment == 'right') ? 'selected="selected"' : ''; ?>>Right</option>
    </select>
  </div>
  <div class="form-group">
    <?php echo $form->label('class', t('Class')); ?>
    <select name="class">
      <option value="btn-primary" <?php echo (empty($class) || $class == 'btn-primary') ? 'selected="selected"' : ''; ?>>Primary</option>
      <option value="btn-secondary" <?php echo ($class == 'btn-secondary') ? 'selected="selected"' : ''; ?>>Secondary</option>
    </select>
  </div>
</fieldset>

<script type="text/javascript">
$(function() {
    $('div.ccm-block-feature-select-icon').on('change', 'select', function() {
        var $preview = $('i[data-preview=icon]');
            icon = $(this).val();

        $preview.removeClass();
        if (icon) {
            $preview.addClass('fa fa-' + icon);
        }
    });

    $('select[data-select=feature-link-type]').on('change', function() {
       if ($(this).val() == '1') {
           $('div[data-select-contents=feature-link-type-internal]').show();
           $('div[data-select-contents=feature-link-type-external]').hide();
       }
       if ($(this).val() == '2') {
           $('div[data-select-contents=feature-link-type-internal]').hide();
           $('div[data-select-contents=feature-link-type-external]').show();
       }
    }).trigger('change');
});
</script>

<style type="text/css">
  div.ccm-block-feature-select-icon {
    position: relative;
  }
  div.ccm-block-feature-select-icon i {
    position: absolute;
    right: 15px;
    top: 10px;
  }
</style>