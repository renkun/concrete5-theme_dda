<?php defined('C5_EXECUTE') or die("Access Denied."); 
  if ($externalLink) {
    if (!preg_match('/^http/', $externalLink))
      $href = 'http://'.$externalLink;
    else
      $href = $externalLink;
    $target = '_blank';
  }
  else {
    $href = Page::getById($internalLinkCID)->getCollectionPath();
    $target = '_self';
  }
  if (!$textLine2) {
    if ($alignment == 'center') {
      echo '<div class="text-center">';
    }
    else {
      $class .= ' pull-'.$alignment;
    }
  }
?>

<a href="<?php echo $href ?>" target="<?php echo $target ?>" class="btn <?php echo $class ?>">
  <?php
    if ($text && $textLine2) {
     echo '<div>'.$text.'</div>';
     echo '<div>'.$textLine2.'</div>';
    }
    else {
      echo $text;
    }
  ?>
</a>

<?php  if (!$textLine2 && $alignment == 'center') { ?>
</div>
<?php } ?>