<?php
  defined('C5_EXECUTE') or die("Access Denied.");
  $is_edit = Page::getCurrentPage()->isEditMode();
 ?>
<div id="carousel-<?php echo $bID ?>" class="carousel fader"
    data-ride="carousel"
    data-interval="<?php echo ($is_edit ? false : $speed) ?>"
    data-pause="hover"
    data-wrap="true"
  >
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php for ($i = 0; $i < count($rows); $i++): ?>
    <li data-target="#carousel-<?php echo $bID ?>" data-slide-to="<?php echo $i ?>" <?php if ($i==0) echo 'class="active"' ?>></li>
    <?php endfor; ?>
  </ol>

  <div class="carousel-inner carousel-inner-spotlight" role="listbox">
    <?php
      $count = 0;
      foreach ($rows as $row):
        $f = File::getByID($row['fID']);
    ?>
    <div class="item bg<?php if($count==0) echo ' active'?> container-fluid">
      <div class="row">
        <div class="col-sm-4" id="img-section">
          <img src="<?php echo $f->getRelativePath() ?>" class="img-responsive" />
        </div>
        <div class="col-sm-8" id="content-section">
          <div class="carousel-caption">
            <?php if ($tagLine) { ?>
              <div class="tag-line h5"><?php echo $tagLine ?></div>
            <?php } ?>
              <div class="heading h3"><?php echo $row['heading'] ?></div>
            <?php if ($row['subHeading']) { ?>
              <div class="sub-heading small"><?php echo $row['subHeading'] ?></div>
            <?php } ?>
            <?php if ($row['cta'] && ($row['linkedPageCID'] || $row['externalURL'])) {
              if ($row['externalURL']) {
                if (!preg_match('/^http/', $row['externalURL']))
                  $href = 'http://'.$row['externalURL'];
                else
                  $href = $row['externalURL']; ?>
                <a class="btn btn-secondary" href="<?php echo $href ?>" target="_blank"><?php echo $row['cta'] ?></a>
            <?php }
              elseif ($row['linkedPageCID'] != 0) { 
		$page = Page::getById($row['linkedPageCID']); ?>
              <a class="btn btn-primary" href="<?php echo $page->getCollectionPath() ?>"><?php echo $row['cta'] ?></a>
            <?php
              }
            } ?>
          </div>
        </div>
      </div>
    </div>
    <?php $count++; endforeach; ?>
  </div>

  <a class="left carousel-control" href="#carousel-<?php echo $bID ?>" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-<?php echo $bID ?>" role="button" data-slide="next">
    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<?php /*
<script>
 $(document).ready(function() {
    function thisHeight(){
      return $(this).height();
    }
    $(".carousel-inner-spotlight").height(function() {
      return Math.max.apply(Math, $(this).find(".item").map(thisHeight));
    });
  });
</script> */ ?>
