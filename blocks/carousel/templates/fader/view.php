<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="carousel slide carousel-fade" id="carousel-main">
  <a href="#main" class="next-section"><span class="glyphicon glyphicon-triangle-down" aria-hidden="true"></span></a>

  <div class="carousel-inner" role="listbox">
    <?php
      $count = 0;
      foreach ($rows as $row): 
        $f = File::getByID($row['fID']);
        if ($count == 0 && !isset($container_height)) {
          $container_height = $f->getAttribute('height');
        }
    ?>
    <div class="item bg<?php if($count==0) echo ' active'?>" style="background-image: url('<?php echo $f->getRelativePath() ?>');"> 
      <div class="carousel-caption">
        <div class="h1"><?php echo $row['heading'] ?></div>
        <?php if ($row['subHeading']) { ?>
        <div class="h4"><?php echo $row['subHeading'] ?></div>
        <?php } ?>
      </div>
    </div>
    <?php $count++; endforeach; ?>
  </div>
    <a class="left carousel-control hidden" href="#carousel-main" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control hidden" href="#carousel-main" role="button" data-slide="next">
    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


