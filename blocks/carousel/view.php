<?php 
  defined('C5_EXECUTE') or die("Access Denied."); 
  $is_edit = Page::getCurrentPage()->isEditMode();
  if ($maxHeight) {
    $container_height = $maxHeight; ?>
    <style type="text/css">
    .carousel {
      max-height: <?php echo $maxHeight; ?>px;
    }
    </style>
<?php } ?>
<div id="carousel-<?php echo $bID ?>" 
     class="carousel slide" 
     data-ride="carousel" 
     data-interval="<?php echo ($is_edit ? false : $speed) ?>" 
     data-pause="hover"
     data-wrap="true">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php for ($i = 0; $i < count($rows); $i++): ?>
    <li data-target="#carousel-<?php echo $bID ?>" data-slide-to="<?php echo $i ?>" <?php if ($i==0) echo 'class="active"' ?>></li>
    <?php endfor; ?>
  </ol>
  <a href="#section-1" class="next-section"><span class="glyphicon glyphicon-triangle-down" aria-hidden="true"></span></a>

  <div class="carousel-inner" role="listbox">
    <?php
      $count = 0;
      foreach ($rows as $row): 
        $f = File::getByID($row['fID']);
        if ($count == 0 && !isset($container_height)) {
          $container_height = $f->getAttribute('height');
        }
    ?>
    <div class="item bg<?php if($count==0) echo ' active'?>" style="background-image: url('<?php echo $f->getRelativePath() ?>');"> 
      <div class="carousel-caption">
        <div class="h1"><?php echo $row['heading'] ?></div>
        <?php if ($row['subHeading']) { ?>
        <div class="h4"><?php echo $row['subHeading'] ?></div>
        <?php } ?>
        <?php if ($row['cta'] && ($row['linkedPageCID'] || $row['externalURL'])) {
          if ($row['externalURL']) {
            if (!preg_match('/^http/', $row['externalURL']))
              $href = 'http://'.$row['externalURL'];
            else
              $href = $row['externalURL']; ?>
            <a class="btn btn-secondary" href="<?php echo $href ?>" target="_blank"><?php echo $row['cta'] ?></a>
        <?php }
          else if ($row['linkedPageCID']) {
            $page = Page::getById($row['linkedPageCID']);
         ?>
          <a class="btn btn-secondary" href="<?php echo $page->getCollectionPath() ?>"><?php echo $row['cta'] ?></a>
        <?php 
          }
        } ?>
      </div>
    </div>
    <?php $count++; endforeach; ?>
  </div>

  <a class="left carousel-control" href="#carousel-<?php echo $bID ?>" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-<?php echo $bID ?>" role="button" data-slide="next">
    <span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> 
</div>
<?php if ($tagLine) { ?>
<div class="tag-line primary-background">
  <h4><?php echo $tagLine ?></h4>
</div>
<?php } ?>

<?php 
  if ($is_edit) { ?>
<style type="text/css">
  .carousel {
    height: <?php echo $container_height ?>px;
  }
</style>
<?php } ?>


