<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<fieldset>
  <legend>Merchant Page List</legend>
    <div class="form-group">
      <?php echo $form->label('blockTitle', t('Title'))?>
      <?php echo $form->text('blockTitle', $blockTitle); ?>
    </div>
    <div class="form-group">
      <?php echo $form->label('merchantCategory', t('Category'))?>
      <?php if(is_array($categories)) { ?>
        <select class="form-control" name="merchantCategory">
          <option value=""><?php echo t('Select') ?></option>
          <?php foreach ($categories as $cat) { ?>
          <option value="<?php echo $cat ?>" <?php if ($cat == $merchantCategory) { ?> selected <?php } ?>>
            <?php echo $cat ?>
          </option>
        <?php } ?>
        </select>
      <?php } ?>
    </div>
    <div class="form-group">
      <?php echo $form->label('buttonText', t('Button Text'))?>
      <?php echo $form->text('buttonText', ($buttonText != '' ? $buttonText : 'Learn More')); ?>
    </div>
</fieldset>