<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<footer id="visual-footer">
  <div class="container">
    <div class="row">
      <div class="col-md-2 hidden-xs hidden-sm">
        <a href="//novumproductions.com" target="_blank" class="novum-productions">Design by Novum Productions</a>
      </div>
      <div class="col-sm-12 col-md-5">
        <?php
          $a = new GlobalArea('Footer Copy');
          $a->display();
        ?>
      </div>
      <div class="col-sm-6 col-md-3">
        <?php
          $a = new GlobalArea('Footer Social');
          $a->display();
        ?>
      </div>
      <div class="col-sm-6 col-md-2">
        <?php
          $a = new GlobalArea('Footer Contact');
          $a->display();
        ?>
      </div>
      <div class="col-sm-12 visible-sm visible-xs">
        <a href="//novumproductions.com" target="_blank" class="novum-productions">Design by Novum Productions</a>
      </div>
    </div>
  </div>
</footer>
<?php $this->inc('elements/footer_bottom.php');?>