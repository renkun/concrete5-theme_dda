<?php

namespace Concrete\Package\ThemeDda\Controller\SinglePage\Dashboard\System\Basics;
use \Concrete\Core\Page\Controller\DashboardPageController;
use Config;
use Loader;
use \File;

class DdaThemeSettings extends DashboardPageController {

  public function view() {
    $this->set('ig_client_id', h(Config::get('themedda.instagram-client-id')));
    $this->set('ig_user_id', h(Config::get('themedda.instagram-user-id')));
    $this->set('ig_user_name', h(Config::get('themedda.instagram-user-name')));
    $this->set('google_api_key', h(Config::get('themedda.google-api-key')));
    $this->set('merchant_search', h(Config::get('themedda.merchant-search')));
    $this->set('merchant_header_img', h(Config::get('themedda.merchant-header-img')));
  }

  public function theme_settings_saved() {
    $this->set('message', t("Your site's settings have been saved."));
    $this->view();
  }

  public function update_theme_settings() {
    if ($this->token->validate("update_theme_settings")) {
      if ($this->isPost()) {
        if (!$this->post('INSTAGRAM-USER-NAME') || !$this->post('INSTAGRAM-CLIENT-ID')) {
          $this->error->add('You must specify an Instagram client ID and a username.');
        } else {
          Config::save('themedda.instagram-client-id', $this->post('INSTAGRAM-CLIENT-ID'));
          if ($ig_user_id = $this->find_user_id($this->post('INSTAGRAM-USER-NAME'), $this->post('INSTAGRAM-CLIENT-ID'))) {
            Config::save('themedda.instagram-user-id', $ig_user_id);
            Config::save('themedda.instagram-user-name', $this->post('INSTAGRAM-USER-NAME'));
            Config::save('themedda.google-api-key', $this->post('GOOGLE-API-KEY'));
            Config::save('themedda.merchant-search', $this->post('merchant-search'));
            Config::save('themedda.merchant-header-img', $this->post('merchant-header-img'));
            $this->redirect('/dashboard/system/basics/dda_theme_settings','theme_settings_saved');
          }
          else {
            $this->error->add('Unable to find user ID for Instagram user '.$this->post('INSTAGRAM-USER-NAME'));
          }
        }
      }
    } else {
      $this->error->add($this->token->getErrorMessage());
    }
    $this->view();
  }

  public function getFileID() {
    if ($file_id = Config::get('themedda.merchant-header-img')) {
      error_log('FILE ID: '.$file_id);
      return $file_id;
    }
    return false;
  }

  public function getFileObject() {
    error_log('getFileObject');
    return File::getByID($this->getFileID());
  }


  protected function find_user_id($username, $client_id) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.instagram.com/v1/users/search?q=$username&client_id=$client_id");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch); 
    $result = json_decode($result);
    foreach ($result->data as $user) {
      if ($user->username == $username)
        return $user->id;
    }
    return false;
  }
}